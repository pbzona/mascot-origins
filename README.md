# Mascot Origins

This theme is specifically for the [Mascot Origins](https://mascotorigins.com) website. It is likely missing several key features that you'd expect in a robust Ghost theme, so is probably not suitable to install for your personal blog.

Not accepting PRs at the moment, but feel free to modify on your own :)